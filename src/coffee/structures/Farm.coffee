class Farm extends Fortress.Structure
  @size = [2, 2]
  constructor: ->
    super
    @time_to_build = 4
    @fields = []
    for x in [0...Farm.size[0]]
      for y in [0...Farm.size[1]]
        continue if x == 0 && y == 0
        position = [@x + x, @y + y, @z]
        field = new Fortress.Field position, @civ
        field.setFarm this
        @addField field
        @civ.world.placeStructureScaffold field
  resourcesRequired: ->
    [
      [Fortress.WoodPile, 50]
    ]
  description: ->
    "It's a farm.  Grow stuff."
  addField: (field) ->
    @fields.push field

class FarmView extends Fortress.StructureView
  constructor: -> 
    super
    @image = "img/gohome.png"
    @z++
    
class Field extends Fortress.Structure
  constructor: ->
    super
    @time_to_build = 2
    @stages = [["Planting", 30], ["Growing", 50], ["Harvesting", 80], ["Fallow", 90]]
    @tick_count = 0
    @cycle_length = _.last(@stages)[1]
    @fruit_count = 20
    @current_stage = @stages[0][0]
  setFarm: (@farm) ->
  description: ->
    console.log "field description: #{@current_stage}"
    "Fields are where things actually grow!  Current stage: #{@current_stage}"
  tick: ->
    super
    if @built
      proper_stage = (_.find @stages, (stage) =>
        return (@tick_count % @cycle_length) < stage[1]
      )[0]
      if @current_stage != proper_stage
        @current_stage = proper_stage
      @tick_count++
class FieldView extends Fortress.StructureView
  constructor: ->
    super
    @image = "img/field.barren.png"
    
window.Fortress.Farm = Farm
window.Fortress.FarmView = FarmView
window.Fortress.Field = Field
window.Fortress.FieldView = FieldView

