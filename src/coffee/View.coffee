class View
  # TODO remove @world, it can be inferred from @model
  constructor: (@model, @world=@model.world) ->
    @needs_repaint = true
    @condemned = false
    @destroyed = false
    @img = null # the actual Substrate image
    @image_drawn = false
    
    # these should be overridden in subclasses
    @image = null # the image source
    @z = 1
  paint: (force=false) ->
    if !@condemned
      if !@image_drawn
        @image_drawn = true
        canvas = @world.views[0].canvas
        @img = canvas.substrate.drawImage
          paint: false
          src: @image
          z: @z
        force = true
        
    if @model.destroyed || (@condemned && @image_drawn)
      @destroyed = true
      @image_drawn = false
      
      @img.destroy()
      force = true
      
    if @needs_repaint || force
      @needs_repaint = false

      if @world.worldCoordInViewport @model.x, @model.y
        @img.set
          x: @model.x
          y: @model.y
          opacity: @opacity
        @img.paint()

window.Fortress.View = View
