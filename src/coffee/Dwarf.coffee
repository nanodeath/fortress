class Dwarf extends Fortress.Creature
  constructor: ->
    super

  buildPersonality: ->
    
    @fear = 0
    @hunger = 0
    @boredom = 0
    @sleepiness = 0
    @fatigue = 0
    
    # level of @boredom before they wander (assuming no tasks)
    @wander_threshold = Fortress.Util.randInt(5, 10)
    
    # level of @fear before they run home
    @fear_threshold = 20
    
    # multiplier by which distance from home is multiplied
    # to calculate fear
    @timid_factor = 0.5
    @safety_range = 1
    
  processNeeds: ->
    if @task.length == 0
      @boredom++
      if @boredom >= @wander_threshold
        tile = null
        while !tile? || !tile.walkable this
          new_x = @x + Fortress.Util.randInt(-3, 3)
          new_y = @y + Fortress.Util.randInt(-3, 3)
          tile = @world.getTile new_x, new_y, @z
        
        @doTaskNow new Fortress.WalkTask(@world, tile.x, tile.y)
        #@speak "Think I'll walk to #{new_x},#{new_y}"
    else
      @boredom-- if @boredom > 0  
    @hunger += 2
    @sleepiness++
    distance_from_home = @distanceFromSafety()
    if distance_from_home <= @safety_range
      @fear -= 1 if @fear > 0
      @fear = 0 if @fear < 0
    else if @task.length == 0
      @fear += (distance_from_home - @safety_range) * @timid_factor
      
    if @fear > @fear_threshold
      @discardTasks()
      @doTaskNow new Fortress.WalkTask(@world, @civ.x, @civ.y)
      @doTaskLater new Fortress.WaitUntilStatTask @world, "fear", "<1"
      
    #console.log "fear is now #{@fear} (distance from home is #{distance_from_home})"
    
  discardTasks: ->
    tl = @civ.task_lord
    for task in @task when !task.ephemeral
      tl.queue task
        
    @task = []
  distanceFromSafety: ->
    Fortress.Util.distance @x, @y, @civ.x, @civ.y
      
class DwarfView extends Fortress.CreatureView
  @_image_counter = 0
  @images = ["img/dwarf.png", "img/dwarf.blue.png"]
  constructor: ->
    super
    @image = DwarfView.images[(DwarfView._image_counter++) % DwarfView.images.length]

window.Fortress.Dwarf = Dwarf
window.Fortress.DwarfView = DwarfView
