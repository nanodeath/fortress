class Canvas
  constructor: (@id) ->
    $ => 
      @el = $("#" + id)
      @el.substrate {grid_size: 32}
      @substrate = @el.data("substrate")
      @el.bind "click", (ev) => @click ev
      {top: @top, left: @left} = @el.position()
  click: (ev) ->
    adjusted_x = ev.pageX - @left
    adjusted_y = ev.pageY - @top
    @el.trigger "user_clicked", [[adjusted_x, adjusted_y]]
    console.warn "you clicked at (#{adjusted_x}, #{adjusted_y})"
    
window.Fortress.Canvas = Canvas
