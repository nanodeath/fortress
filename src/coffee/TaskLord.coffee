class TaskLord
  constructor: ->
    @labor_map = {}
    @backlog = []
    
  registerWorker: (worker) ->
    for labor_type in worker.labors
      workers = @labor_map[labor_type]
      if !workers?
        workers = []
        @labor_map[labor_type] = workers
      workers.push worker
  
  queue: (task) ->
    task.age = 0
    @backlog.push task
  
  # iterate over all backlog items, looking for workers
  ###
   TODO
   I'd like to implement task distribution as a PULL mechanism instead of PUSH 
   -- this way, a long list of tasks won't get dumped on a single worker
   since other workers will be lining up asking for tasks.
   
   First thought: workers should be given a task as soon as they ask for one (before this
   method returns), but then you lose the ability to prioritize dwarves (i.e. if two dwarves
   ask for the same labor, the closer one perhaps should get the job).
   
   Second thought: workers don't receive a task until after the TaskLord ticks, after all the
   creatures have ticked.  They'll start on their next turn.
   
   Pros:
    Improved task distribution
   Cons:
    Need to group tasks by type (should be doing this already though)
  ###
  tick: ->
    new_backlog = []
    for task in @backlog
      continue if task.is_done
      labor_type = task.constructor.name
      workers = @labor_map[labor_type]
      candidates = []
      # check for any workers of the available labor
      if workers?
        upper = workers.length
        for idx in [0...upper]
          # iterate over each worker
          worker = workers[idx]
          # double-check to make sure the worker can actually perform the required labor
          if labor_type in worker.labors
            candidates.push worker
          # if the worker can't, remove them from the list and adjust the counters
          else
            workers.splice idx, 1
            idx--
            upper--

        # later we'll use more advanced heuristics
        worker = candidates[0]
        if worker?
          console.log "giving worker a job of type #{labor_type}"
          worker.task.push task
        else
          console.log "no worker available for #{labor_type}"
          task.age++
          new_backlog.push task
    @backlog = new_backlog


window.TaskLord = TaskLord
