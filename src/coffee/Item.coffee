class Item extends Fortress.Model
  constructor: (@x, @y, @z, @amount, civ) ->
    super(civ)
    @pickupable = true
    @packed = false
    @maximum_stack_size = 1
    @container = null
    
  pack: ->
    PageBus.publish("#{@channel}.pack", this)
    @packed = true
  unpack: ->
    PageBus.publish("#{@channel}.unpack", this)
    @packed = false
  setAmount: (newAmount) ->
    if newAmount <= 0
      # destroy
      throw new Error("not supported yet")
    @amount = newAmount
  setContainer: (@container) ->
    
  @findClosestItem = (item_type, destination_model) ->
    # look in storage facilities first
    structures = Fortress.StorageStructure.findStorageFor(new item_type(), destination_model)
    item = _.detect structures, (structure_item) ->
      structure_item instanceof item_type
    return item if item?
    
    # then look on the ground
    items = destination_model.civ.findModelsByClass item_type, Item
    if items.length > 0
      # TODO something smarter
      items[0]
    else
      null
    
class ResourcePile extends Item

class WoodPile extends ResourcePile
  @description = "A pile of wood"
  constructor: ->
    super
    @maximum_stack_size = 100
  description: ->
    "A pile of wood of size #{@amount}"
  getActions: ->
    [Fortress.TakeItem, Fortress.FetchAndStoreItem]
  split: (amount) ->
    if amount < @amount - 1
      newWood = new @constructor(@x, @y, @z, amount, @civ)
      @setAmount @amount - amount
      newWood
    else
      null
      
class Apple extends ResourcePile
  constructor: ->
    super
    @maximum_stack_size = 100
  description: ->
    "A pile of apples of size #{@amount}"

class ItemView extends Fortress.View
  constructor: ->
    super
    @z = 40
    @_eventModelPackedSid = PageBus.subscribe "#{@model.channel}.pack", this, @_event_ModelPacked
    @_eventModelUnpackedSid = PageBus.subscribe "#{@model.channel}.unpack", this, @_event_ModelUnpacked
    
  _event_ModelPacked: ->
    @condemned = true
  _event_ModelUnpacked: ->
    @condemned = false
    sector = @world.getSectorFor @model.x, @model.y, @model.z
    sector.registerView this
    
class ResourcePileView extends ItemView
    
class WoodPileView extends ResourcePileView
  constructor: ->
    super
    @image = "img/wood.png"
    

window.Fortress.Item = Item
window.Fortress.ItemView = ItemView
window.Fortress.WoodPile = WoodPile
window.Fortress.Apple = Apple
window.Fortress.ResourcePile = ResourcePile
window.Fortress.WoodPileView = WoodPileView
