class FirePit extends Fortress.Structure
  constructor: ->
    super
    @time_to_build = 1
  
class FirePitView extends Fortress.StructureView
  constructor: -> 
    super
    @image = "img/fire.png"
    
window.Fortress.FirePit = FirePit
window.Fortress.FirePitView = FirePitView
