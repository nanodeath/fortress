class Civilization
  constructor: (@name, @x, @y, @z, @world) ->
    Civilization.get[@name] = this
    @task_lord = new TaskLord
    @models =
      structures: []
      creatures: []
      items: []
    
  tick: ->
    @task_lord.tick()
    
  getElementKey: (el) ->
    if(el instanceof Fortress.Creature)
      "creatures"
    else if(el instanceof Fortress.Structure)
      "structures"
    else if(el instanceof Fortress.Item)
      "items"
    else
      "unknown"
      
  registerModel: (el) ->
    element_type = @getElementKey el
    arr = @models[element_type]
    unless arr?
      throw new Error("element_type '#{element_type}' does not exist")
    arr.push el
    
  findModelsByClass: (klass, base_class) ->
    element_type = if base_class == Fortress.Creature
      "creatures"
    else if base_class == Fortress.Structure
      "structures"
    else if base_class == Fortress.Item
      "items"
    arr = @models[element_type]
    unless arr?
      throw new Error("element_type '#{element_type}' does not exist")    
    _.filter arr, (model) ->
      model instanceof klass && !model.destroyed
    
    
Civilization.get = {}

window.Fortress.Civilization = Civilization
