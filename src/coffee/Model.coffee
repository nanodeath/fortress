class Model
  @id = 0
  constructor: (@civ) ->
    this.constructor.next_id = 1 if !this.constructor.next_id?
    @id = "#{this.constructor.name}_#{this.constructor.next_id++}"
    @channel = "model.#{@id}"
    @destroyed = false
    
    # overridden in subclasses
    #@x = 0
    #@y = 0
    #@z = 0
  canMove: ->
    false
  getActions: ->
    []
  destroy: ->
    @destroyed = true
    
  atSamePositionAs: (other_model) ->
    @x == other_model.x && @y == other_model.y && @z == other_model.z
    
  atPosition: (x, y, z) ->
    @x == x && @y == y && @z == z
    
window.Fortress.Model = Model
