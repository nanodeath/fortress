class WorldView
  constructor: (@world, @canvas) ->
    
  # Whether the sector should be ticked and drawn or not
  isSectorNearViewport: (sector) ->
    # hax for now
    true
    
  paint: ->
    # later we can decide which sectors to paint
    for sector_column in @world.sectors
      for sector_row in sector_column
        for sector in sector_row
          sector.paint()
      
    @paintGUI()
    
  paintGUI: ->
    @gui.paint()

  ###
  registerView: (view) ->
    view.world = @world
    @views.push view
  ###
  registerGUI: (@gui) ->
    @gui.world = @world
    @gui.canvas = @canvas

window.Fortress.WorldView = WorldView
