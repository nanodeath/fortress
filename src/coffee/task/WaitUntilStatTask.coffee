class WaitUntilStatTask extends Fortress.Task
  constructor: (world, @stat, threshold) ->
    super(world)
    if typeof threshold is "string"
      @operand = threshold[0]
      @operand += threshold[1] if threshold[1] == "="
      @threshold = parseFloat threshold.substr(@operand.length)
    else
      throw new Error "not implemented"
    
  perform: (creature) ->
    switch @operand
      when "<"
        @is_done = true if creature[@stat] < @threshold
      when "<="
        @is_done = true if creature[@stat] <= @threshold
      when "=="
        @is_done = true if creature[@stat] == @threshold        
      when ">="
        @is_done = true if creature[@stat] >= @threshold
      when ">"
        @is_done = true if creature[@stat] > @threshold
      else
        @is_done = true

window.Fortress.WaitUntilStatTask = WaitUntilStatTask
