class DropOffItemTask extends Fortress.ModelTask
  # destination can either be a Structure or a position array
  constructor: (@item, @destination) ->
    super(@item)
    if @destination instanceof Fortress.Structure
      @perform = @_performAgainstStructure
    else
      @perform = @_performAgainstPosition
  
  # one of the following two methods will be aliased as #perform, depending
  # on the type of the destination
  _performAgainstStructure: (creature) ->
    if !creature.atSamePositionAs(@destination)
      creature.doTaskNow new Fortress.WalkTask(creature.world, @destination.x, @destination.y, @destination.z)
    else
      creature.removeFromBackpack @item, @destination
      @is_done = true

  _performAgainstCoordinates: (creature) ->
    if !creature.atPosition(@destination[0], @destination[1], @destination[2])
      creature.doTaskNow new Fortress.WalkTask(creature.world, @destination[0], @destination[1], @destination[2])
    else
      creature.removeFromBackpack @item
      @is_done = true

window.Fortress.DropOffItemTask = DropOffItemTask
