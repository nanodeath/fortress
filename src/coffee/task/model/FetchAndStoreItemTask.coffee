class FetchAndStoreItemTask extends Fortress.FetchAndTransferItemTask
  getDestination: (creature) ->
    Fortress.StorageStructure.findStorageFor @model, creature
  hasItem: (creature) ->
    creature.hasInBackpack @model
  findItem: ->
    @model
  deliverItem: (destination, creature) ->
    creature.removeFromBackpack @model, destination, @amount

window.Fortress.FetchAndStoreItemTask = FetchAndStoreItemTask
