class BuildTask extends Fortress.ModelTask
  constructor: ->
    super
    @effort = 10
    @resource_tasks_queued = false
    @sector = @model.world.getSectorFor @model.x, @model.y, @model.z
    @started = false

  perform: (creature) ->
    if creature.x != @model.x || creature.y != @model.y
      creature.doTaskNow new Fortress.WalkTask(@model.world, @model.x, @model.y)
    else
      if !@started
        @started = true
        console.log "starting, should destroy resources"
        # TODO destroy resources
      if @model.built
        @is_done = true
      else
        new_health = @model.health + 100 / @model.time_to_build
        if new_health >= 100
          @model.built = true
          @is_done = true
          if new_health > 100
            new_health = 100
          creature.speak "Zug zug!"
        @model.health = new_health
        
window.Fortress.BuildTask = BuildTask
