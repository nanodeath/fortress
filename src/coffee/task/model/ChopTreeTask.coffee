# TODO this is stupid, shouldn't be a location task, should be a model task
class ChopTreeTask extends Fortress.HarvestResourceTask
  @title = "Chop down tree"
  constructor: (@tree) ->
    super
    @wood_chopped = 0
    
  perform: (creature) ->
    if !creature.atSamePositionAs @tree
      creature.doTaskNow new Fortress.WalkTask(@world, @tree.x, @tree.y, @tree.z)
    else
      if @wood_chopped < @tree.wood
        @wood_chopped += 25
      if @wood_chopped >= @tree.wood
        @is_done = true
        @tree.destroy()
        wp = new Fortress.WoodPile(@tree.x, @tree.y, @tree.z, @tree.wood, creature.civ)
        @world.buildComponent wp
        creature.civ.task_lord.queue new Fortress.FetchAndStoreItemTask(wp)
      console.log "wood chopped: #{@wood_chopped}"

window.Fortress.ChopTreeTask = ChopTreeTask
