class TakeItemTask extends Fortress.ModelTask
  perform: (creature) ->
    if !creature.atSamePositionAs(@model)
      # walk to item
      creature.doTaskNow new Fortress.WalkTask(@model.world, @model.x, @model.y)    
    else
      # we're on the square, but we don't have it yet, so grab it!
      creature.putInBackpack @model
      @is_done = true

window.Fortress.TakeItemTask = TakeItemTask
