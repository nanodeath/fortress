class FetchAndTransferItemTask extends Fortress.ModelTask
  constructor: (item, @amount=item.amount) ->
    super
  perform: (creature) ->
    if @hasItem creature
      destination = @getDestination creature
      if !destination?
        console.warn "argh I don't know where to put this %o", @model
        creature.removeFromBackpack @model, @amount
        @is_done = true
      if !@is_done
        if !creature.atSamePositionAs destination
          creature.doTaskNow new Fortress.WalkTask(@world, destination.x, destination.y, destination.z)
        else
          @deliverItem destination, creature
          @is_done = true
    else
      item = @findItem creature
      if !creature.atSamePositionAs item
        # walk to item
        creature.doTaskNow new Fortress.WalkTask(@world, item.x, item.y, item.z)
      else 
        # we're on the square, but we don't have it yet, so grab it!
        
        if item.container instanceof Fortress.Structure
          item.container.removeFromContents item, creature, @amount
        else if !item.container? # it's on the ground
          creature.putInBackpack item, @amount
        else # uh, not sure, it's on another creature...?
          console.error "item found, but on another creature?"

  getDestination: ->
    throw new Error("abstract")
  hasItem: ->
    throw new Error("abstract")
  findItem: ->
    throw new Error("abstract")
  deliverItem: (destination, creature) ->
    throw new Error("abstract")

window.Fortress.FetchAndTransferItemTask = FetchAndTransferItemTask
