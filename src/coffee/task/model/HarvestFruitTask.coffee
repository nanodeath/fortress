class HarvestFruitTask extends Fortress.FetchAndStoreItemTask
  @title = "Harvest fruit"
  # precondition: task is only assigned if tree has fruit
  constructor: (@tree) ->
    super(@tree.getFruit())

window.Fortress.HarvestFruitTask = HarvestFruitTask
