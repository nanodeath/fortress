class FetchResourceForConstructionTask extends Fortress.FetchAndTransferItemTask
  constructor: (@structure, @resource_class, @amount) ->
    @step = 0
    
  getDestination: ->
    @structure
  hasItem: (creature) ->
    current_amount = 0
    creature.eachBackpack (item) =>
      if item instanceof @resource_class
        current_amount += item.amount
    current_amount >= @amount
  findItem: ->
    Fortress.Item.findClosestItem @resource_class, @structure
  deliverItem: (_ignore_, creature) ->
    amount_needed = @amount
    removal_list = []
    creature.eachBackpack (item) =>
      if amount_needed > 0 && item instanceof @resource_class
        amount_to_use = if item.amount > amount_needed
          amount_needed
        else
          item.amount
        amount_needed -= amount_to_use
        removal_list.push [item, amount_to_use]
    for item in removal_list
      creature.removeFromBackpack item[0], @structure, item[1]
    if amount_needed > 0
      console.log "oops, came up #{amount_needed} short of this resource..."
      creature.civ.task_lord.queue new Fortress.FetchResourceForConstructionTask(@structure, @resource_class, amount_needed)
    true

window.Fortress.FetchResourceForConstructionTask = FetchResourceForConstructionTask
