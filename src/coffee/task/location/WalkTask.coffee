class WalkTask extends Fortress.LocationTask
  perform: (creature) ->
    x = if @x > creature.x then 1
    else if @x < creature.x then -1
    else 0
    y = if @y > creature.y then 1
    else if @y < creature.y then -1
    else 0
    z = 0
    creature.move x, y, z
    if @x == creature.x and @y == creature.y
      #creature.speak "I've reached my destination."
      @is_done = true
      
window.Fortress.WalkTask = WalkTask
