class StorageStructure extends Fortress.Structure
  @findStorageFor = (item, model) ->
    storage_structures = model.civ.findModelsByClass StorageStructure, Fortress.Structure
    candidate_structures = _.filter storage_structures, (structure) ->
      structure.canStore item
    if candidate_structures.length > 0
      # TODO something smarter
      candidate_structures[0]
    else
      null

class Shed extends StorageStructure
  constructor: ->
    super
    @time_to_build = 2
  setHealth: (@health) ->
    super
    if @health == 100
      console.log "shed done!"
  capacity: ->
    5

class ShedView extends Fortress.StructureView
  constructor: -> 
    super
    @image = "img/Warehouse.png"
    
window.Fortress.StorageStructure = StorageStructure
window.Fortress.Shed = Shed
window.Fortress.ShedView = ShedView
