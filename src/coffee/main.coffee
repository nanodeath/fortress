C = new Fortress.Canvas("draw_on_me")
W = new Fortress.World(250, C)
Civ = new Fortress.Civilization("Player 1", 10, 10, 0, W)
firepit = W.buildComponent(new Fortress.FirePit([10, 10, 0], Civ, true))
shed = W.buildComponent(new Fortress.Shed([12, 10, 0], Civ, true))

builda_choppa_dwarf = new Fortress.Dwarf("Builda Choppa", [10, 10, 0], Civ)
builda_choppa_dwarf.addLabor "BuildTask"
builda_choppa_dwarf.addLabor "ChopTreeTask"
W.buildComponent(builda_choppa_dwarf)

collecta_dwarf = new Fortress.Dwarf("Collecta", [11, 10, 0], Civ)
collecta_dwarf.addLabor "FetchAndStoreItemTask"
collecta_dwarf.addLabor "FetchResourceForConstructionTask"
collecta_dwarf.addLabor "HarvestFruitTask"

W.buildComponent(collecta_dwarf)

appleTree = W.buildComponent(new Fortress.TreeApple([20, 5, 0]))


tree = W.buildComponent(new Fortress.TreeDouglasFir([3, 3, 0]))
G = new Fortress.GUI()
W.views[0].registerGUI G
