window.Fortress.Util =
  randInt: (min, max) ->
    max = min unless max?
    min = 0 unless min?
    ret = Math.random()*(max - min + 1) + min
    Math.floor(ret)
  distance: (x1, y1, x2, y2, method) ->
    switch method
      when "pythag"
        Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
      else
        window.Fortress.Util.distance x1, y2, x2, y2, "pythag"
    
