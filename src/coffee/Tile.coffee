class Tile extends Fortress.Model
  constructor: (@x, @y, @z) ->
    super(null)
  walkable: (creature) ->
    true

class Grass extends Tile
  description: ->
    "Green stuff that you walk on"

class Ocean extends Tile
  description: ->
    "Waterrrr!"
  walkable: ->
    false

class TileView extends Fortress.View
  constructor: ->
    super
    @z = 10
  
class GrassView extends TileView
  constructor: -> 
    super
    @image = "img/grass.gif"
    
class OceanView extends TileView
  constructor: -> 
    super
    @image = "img/Ocean.png"

window.Fortress.Tile = Tile
window.Fortress.Grass = Grass
window.Fortress.Ocean = Ocean

window.Fortress.TileView = TileView
window.Fortress.GrassView = GrassView
window.Fortress.OceanView = OceanView
