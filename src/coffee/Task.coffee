class Task
  constructor: (@world) ->
    @is_done = false
    @ephemeral = false
    @effort = 1
  perform: (creature) ->
    throw new Error("abstract")
  
  # whether to hold onto this task, but skip it for now and try
  # again next tick
  pass: (creature) ->
    false

window.Fortress.Task = Task
