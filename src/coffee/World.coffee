class World
  constructor: (@refresh_rate, @canvas) ->
    @creatures = []
    @views = []
    @mode = "select"

    # Populate world
    console.log "populating world"
    @sector_size = 8
    @size = [4, 3, 1] # x, y, z
    @sectors = []
    max_x = @size[0] * @sector_size - 1
    max_y = @size[1] * @sector_size - 1
    for x in [0...@size[0]]
      y_column = []
      for y in [0...@size[1]]
        z_column = []
        for z in [0...@size[2]]
          sector = new Fortress.Sector(x, y, z)
          base_x = x * @sector_size
          base_y = y * @sector_size
          for sector_x in [0...@sector_size]
            for sector_y in [0...@sector_size]
              tile_x = base_x + sector_x
              tile_y = base_y + sector_y
              component = if tile_x == 0 || tile_y == 0 || tile_x == max_x || tile_y == max_y
                new Fortress.Ocean(tile_x, tile_y, z)
              else
                new Fortress.Grass(tile_x, tile_y, z)
              @buildComponent(component, sector)
          z_column.push sector
        y_column[y] = z_column
      @sectors[x] = y_column
    
    @tick_sum = 0
    @view_sum = 0
    @tick_view_count = 0
    # Tick and paint world components
    setInterval =>
      @tick_view_count++
      start = new Date
      @tick()
      tick_length = new Date - start
      @tick_sum += tick_length
      start = new Date
      view.paint() for view in @views
      view_length = new Date - start
      @view_sum += view_length
    , @refresh_rate
    
    # Set up viewport info
    @scale = 32
    @offset = [0, 0] # for when the viewport shifts
    @viewport_size = [1024/@scale, 768/@scale]
    @views.push(new Fortress.WorldView this, @canvas)
    
    # Handle user interaction
    $ =>
      @canvas.el.bind "user_clicked", (ev, position) =>
        
        [world_x, world_y] = @screenToWorld position[0], position[1]
        
        switch @mode
          when "select"
            selection = {x: world_x, y: world_y, z: 0, w: 1, h: 1}
            if @selection? and (selection.x == @selection.x && selection.y == @selection.y)
              @selection = null
            else
              @selection = selection
        view.paintGUI() for view in @views when view.paintGUI
  tick: ->
    # first we tick civilizations
    for civ_name, civ of Fortress.Civilization.get
      civ.tick()
      
    # later we can decide which sectors to tick
    for sector_column in @sectors
      for sector_row in sector_column
        for sector in sector_row
          sector.tick()
      
  getSector: (x, y, z) ->
    @sectors[x]?[y]?[z]
  getSectorFor: (world_x, world_y, world_z) ->
    s = @sector_size
    @sectors[Math.floor(world_x / s)]?[Math.floor(world_y / s)]?[Math.floor(world_z / s)]
      
  screenToWorld: (world_x, world_y) ->
    # Compensate for scale
    world_x /= @scale
    world_y /= @scale
    
    # Compensate for viewport position
    world_x -= @offset[0]
    world_y -= @offset[1]
    
    # Round down (to upper left corner of tile)
    world_x = Math.floor world_x
    world_y = Math.floor world_y
    [world_x, world_y]
    
  worldToScreen: (screen_x, screen_y) ->
    screen_x += @offset[0]
    screen_y += @offset[1]
    
    screen_x *= @scale
    screen_y *= @scale
    
    [screen_x, screen_y]
    
  # deprecated  
  getStackFor: (view_component) ->
    if component instanceof TileView
      @stack.terrain
    else if component instanceof CreatureView
      @stack.creatures
    else
      @stack.gui 
    

  worldCoordInViewport: (x, y, z) ->
    x -= @offset[0]
    y -= @offset[1]
    0 <= x < @viewport_size[0] and 0 <= y < @viewport_size[1]
    
  screenCoordInViewport: (x, y, z) ->
    [x, y] = screenToWorld x, y
    worldCoordInViewport x, y, z
  
  buildComponent: (model, sector=@getSectorFor(model.x, model.y, model.z)) ->
    model.world = this
    view = model.constructor.default_view
    if !view?
      # Lookup view by their name (constructor name followed by View)
      view = Fortress[model.constructor.name + "View"]
    if view?
      view = new view(model, this)
    
    model.civ.registerModel model if model.civ?
    
    sector.registerModel model
    sector.registerView view
      
    if model instanceof Fortress.Creature 
      model.civ.task_lord.registerWorker model
      
    [model, view]
    
  placeStructureScaffold: (structure) ->
    @buildComponent structure
    structure
    
  getTile: (x, y, z) ->
    sector = @getSectorFor x, y, z
    return null unless sector?
    sector.find "model", "terrain", (tile) ->
      tile.x == x and tile.y == y and tile.z == z
    
window.Fortress.World = World
