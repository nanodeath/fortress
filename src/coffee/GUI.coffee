class GUI
  constructor: ->
    $ => 
      @info_div = $("div#info")
      @buildings_panel = $("div#buildings_panel")
      
      @buildings_panel.delegate "li.house a", "click", (ev) =>
        console.log "place a house at %o", @world.selection
        position = [@world.selection.x, @world.selection.y, @world.selection.z]
        @world.placeStructureScaffold(new Fortress.House position, Fortress.Civilization.get["Player 1"])
        
      @buildings_panel.delegate "li.shed a", "click", (ev) =>
        position = [@world.selection.x, @world.selection.y, @world.selection.z]
        @world.placeStructureScaffold(new Fortress.Shed position, Fortress.Civilization.get["Player 1"])
        
      @buildings_panel.delegate "li.farm a", "click", (ev) =>
        position = [@world.selection.x, @world.selection.y, @world.selection.z]
        @world.placeStructureScaffold(new Fortress.Farm position, Fortress.Civilization.get["Player 1"])
      
  paint: ->
    if @world.selection?
      selection = @world.worldToScreen @world.selection.x, @world.selection.y
      dimensions = @world.worldToScreen @world.selection.w, @world.selection.h
      
    if @world.selection?
      {x: x, y: y, z: z} = @world.selection
      
      # draw rectangle
      if !@selection_rect
        @selection_rect = @canvas.substrate.drawRectangle
          x: x
          y: y
          z: 100
          height: 1
          width: 1
          strokeWidth: 1
          strokeColor: "red"
      else
        @selection_rect.set "x", x
        @selection_rect.set "y", y
    
      # get selected tile
      tile = @world.getTile(x, y, z)
      
      @info_div.text tile.description()
      
      if !@buildings_panel.is(":visible")
        @buildings_panel.show()
      
      current_html = $("#context_menu_options");
      new_html = $("<div>");
      
      addActions = (model, actions) =>
        ol = $ "<ol>"
        for action in actions
          li = $ "<li><button>#{action.title}</button></li>"
          button = li.find "button"
          button.data "model", model
          button.data "action", action
          
          button.click (ev) =>
            civ = Fortress.Civilization.get["Player 1"]
            civ.task_lord.queue new action(model)
          ol.append li
        ol
      
      sector = @world.getSectorFor x, y, z
      structures_on_tile = sector.findByPos x, y, z, "structures"
      for structure in structures_on_tile
        div = $ "<div>"
        div.append structure.description()
        actions = structure.getActions()
        div.append addActions structure, actions if actions?
        
        if structure.contents.length > 0
          ol = $ "<ol>"
          for item in structure.contents
            li = $ "<li>#{item.description()}</li>"
            ol.append li
          div.append ol
        else if structure instanceof Fortress.StorageStructure
          div.append "No contents"
        
        if structure.permanent_contents.length > 0
          ol = $ "<ol>"
          for item in structure.permanent_contents
            li = $ "<li>#{item.description()}</li>"
            ol.append li
          div.append "Structure parts:"
          div.append ol
          
        
        new_html.append div
      
      items_on_tile = sector.findByPos x, y, z, "items"
      for item in items_on_tile
        div = $ "<div>"
        div.append item.description()
        actions = item.getActions()
        div.append addActions item, item.description, actions if actions?
        
        new_html.append div
      
      if new_html.html() != current_html.children().html()
        console.warn "updating right nav"
        current_html.empty().append(new_html)
    else
      if @buildings_panel.is(":visible")
        @buildings_panel.hide()
        
      # clear rectangle
      if @selection_rect?
        console.warn 'bye!'
        @selection_rect.destroy()
        @selection_rect.paint()
        
        @selection_rect = null
        
    # updating framerate
    $("#average_tick .value").html(Math.round(@world.tick_sum * 10 / @world.tick_view_count) / 10);
    $("#average_paint .value").html(Math.round(@world.view_sum * 10 / @world.tick_view_count) / 10);
      

window.Fortress.GUI = GUI
