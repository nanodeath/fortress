class Structure extends Fortress.Model
  constructor: (position, civilization, @built=false) ->
    super(civilization)
    [@x, @y, @z] = position
    @time_to_build = 1
    @health = 0
    # items being stored inside the building
    @contents = []
    
    # items that compose the building, i.e. can't be removed
    @permanent_contents = []
    
    @building_in_progress = false
    
    # accumulate resources
    resources_required = @resourcesRequired()
    unless @built
      if resources_required.length > 0
        for required_resource in resources_required
          required_resource_type = required_resource[0]
          required_resource_amount = required_resource[1]
          task = new Fortress.FetchResourceForConstructionTask this, required_resource_type, required_resource_amount
          civilization.task_lord.queue task

  tick: ->
    if !@built && !@building_in_progress && @resourcesRequired().length == 0
      @_commenceBuilding()
      
  # precondition: all required resources are already present in the building
  _commenceBuilding: ->
    unless @building_in_progress
      @building_in_progress = true
      task = new Fortress.BuildTask this
      @civ.task_lord.queue task
      
      resources_required = @resourcesRequired()
      for resource in resources_required
        klass = resource[0]
        amount_left = resource[1]
        
        contents = _.clone @contents
        for item in contents
          if item instanceof klass
            amount_to_use = if amount_left < item.amount then amount_left else item.amount
            amount_left -= amount_left
            @convertItemToPermanentComponent item, amount_to_use
            break if amount_left == 0
        if amount_left > 0
          throw new Error("Not sure what happened, but not all resource requirements were met: #{amount_left} of #{klass}")
            
        
  convertItemToPermanentComponent: (item, amount=item.amount) ->
    item_idx = _.lastIndexOf @contents, item
    return false if item_idx < 0
    success = false
    if amount >= item.amount
      model_item = @contents.splice(item_idx, 1)[0]
    else
      model_item = @contents[item_idx].split amount
    @permanent_contents.push model_item
    true
    
  description: ->
    "Some sort of structure"
    
  # Array of [ItemClass, Amount] tuples
  resourcesRequired: ->
    []
  
  setHealth: (@health) ->
    # 0 is destroyed, 100 is maximum
    
  # this should probably be overridden
  capacity: ->
    1

  canStore: (item) ->
    @contents.length < @capacity() && (item instanceof Fortress.Item)
    
  putInContents: (item, amount=item.amount) ->
    if @canStore item
      @contents.push item
      item.setContainer this
      item.x = @x
      item.y = @y
      item.z = @z
      
      if !@built && @_resourcesInPlace()
        @_commenceBuilding()
      
      true
    else
      false
  
  # check to see if resources are in place, so construction can commence
  _resourcesInPlace: ->
    resources_present = @contents
    resources_required = @resourcesRequired()
    resources_to_consume = []
    
    for required_resource in resources_required
      required_resource_type = required_resource[0]
      #required_resource_amount = required_resource[1]
      
      for present_resource in resources_present
        if(present_resource instanceof required_resource_type)
          required_resource[1] -= present_resource.amount
        break if required_resource[1] <= 0
    
    requirements_met = _.all resources_required, (resource) ->
      resource[1] <= 0
    requirements_met
      
  removeFromContents: (item, destination="ground", amount=item.amount) ->
    item_idx = _.lastIndexOf @contents, item
    return null if item_idx < 0
    success = false
    model_item = if amount >= item.amount
      @contents.splice(item_idx, 1)[0]
    else
      @contents[item_idx].split amount
      
    if destination instanceof Fortress.Creature
      success = destination.putInBackpack model_item
    else if destination == "ground"
      item.x = @x
      item.y = @y
      item.z = @z
      item.unpack()
      item.setContainer null
    else
      throw new Error("not sure how to transfer item to #{destination.constructor.name}")
      
    success
  
  contains: (item_type) ->
    _.any @contents, (item) ->
      item instanceof item_type

class StructureView extends Fortress.View
  constructor: ->
    super
    @z = 20

  paint: ->
    if !@model.built
      if !@opacity?
        @needs_repaint = true
        @opacity = 0.25
    else if @opacity?
      @needs_repaint = true
      @opacity = 1.0
    super
  
window.Fortress.Structure = Structure
window.Fortress.StructureView = StructureView

