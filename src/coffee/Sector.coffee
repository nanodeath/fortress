class Sector
  constructor: (@x, @y, @z) ->
    @models =
      terrain: []
      structures: []
      items: []
      creatures: []
    @views =
      terrain: []
      structures: []
      items: []
      creatures: []
      
  # Maps a model or view to its key in this sector object
  getElementKey: (el) ->
    if(el instanceof Fortress.Creature || el instanceof Fortress.CreatureView)
      "creatures"
    else if(el instanceof Fortress.Structure || el instanceof Fortress.StructureView)
      "structures"
    else if(el instanceof Fortress.Tile || el instanceof Fortress.TileView)
      "terrain"
    else if(el instanceof Fortress.Item || el instanceof Fortress.ItemView)
      "items"
    else
      "unknown"
      
  find: (element_supertype, element_type, condition) ->
    arr = switch element_supertype
      when "model" then @models[element_type]
      when "view" then @views[element_type]
    _.find arr, condition
  
  # returns all models on the given position
  findByPos: (x, y, z, element_type="all") ->
    return [] if z != @z
    if element_type == "all"
      @findByPos(x, y, z, "terrain").concat(@findByPos(x, y, z, "structures")).concat(@findByPos(x, y, z, "creatures"))
    else if typeof element_type == "string"
      arr = @models[element_type]
      if !arr?
        throw new Error("element type '#{element_type}' was invalid")
      _.filter arr, (m) ->
        m.x == x && m.y == y
    else if typeof element_type == "function"
      _.filter @findByPos(x, y, z, "all"), (m) ->
        m instanceof element_type

  registerModel: (el) ->
    if el.canMove()
      sid = PageBus.subscribe "#{el.channel}.move", this, (topic, creature) ->
        world = creature.world
        new_sector = world.getSectorFor creature.x, creature.y, creature.z
        if new_sector != this
          @unregisterModel creature
          PageBus.unsubscribe sid
          if new_sector?
            new_sector.registerModel creature
          PageBus.publish "#{el.channel}.left_sector", new_sector
          
    @registerElement "model", el
    
  registerView: (el) ->
    if el.model.canMove
      sid = PageBus.subscribe "#{el.model.channel}.left_sector", this, (topic, new_sector) ->
        @unregisterView el
        PageBus.unsubscribe sid
        if new_sector?
          new_sector.registerView el
    @registerElement "view", el
    
  registerElement: (element_supertype, el) ->
    element_type = @getElementKey el
    arr = switch element_supertype
      when "model" then @models[element_type]
      when "view" then @views[element_type]
      else throw new Error("element_supertype '#{element_supertype}' is not valid; only 'model' and 'view' are")
    unless arr?
      throw new Error("element_type '#{element_type}' does not exist for supertype '#{element_supertype}'")
    already_registered = true
    unless _.indexOf(arr, el) >= 0
      already_registered = false
      arr.push el
    el.sector = this
    already_registered    
        
  unregisterModel: (el) ->
    @unregisterElement "model", el
  unregisterView: (el) ->
    @unregisterElement "view", el
  # unregister an element
  # returns the number of times it was registered in the sector (should be 0 or 1)
  unregisterElement: (element_supertype, el) ->
    element_type = @getElementKey el
    arr = switch element_supertype
      when "model" then @models[element_type]
      when "view" then @views[element_type]
    i = _.indexOf arr, el
    if i >= 0
      arr.splice i, 1
      i = _.indexOf arr, el
      true
    else
      false
    
  tick: ->
    for supertype, group of @models
      len = group.length
      for idx in [0...len]
        model = group[idx]
        model.tick() if model.tick
        if model.sector == this
          if model.destroyed
            @unregisterModel model
            idx--
            len--
        else 
          # moved out of the sector, decrement idx
          idx--
          len--

  paint: ->
    for supertype, group of @views
      len = group.length
      for idx in [0...len]
        view = group[idx]
        view.paint()
        if view.destroyed
          @unregisterView view
          idx--
          len--

window.Fortress.Sector = Sector
