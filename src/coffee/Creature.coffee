class Creature extends Fortress.Model
  @type = 'creature'
  constructor: (@name, position, civilization) ->
    super(civilization)
    [@x, @y, @z] = position
    
    # State and Personality
    @buildPersonality()
    
    @task = []
    @labors = []
    @backpack = []
    
  # takes a string of the class, like BuildTask
  # should probably be the class itself
  addLabor: (labor) ->
    @labors.push labor
  
  doTaskNow: (task) ->
    if @task[0]?.ephemeral
      @task.shift()
    @task.unshift task

  doTaskLater: (task) ->
    @task.push task
    
  move: (x, y, z=0) ->
    @x += x
    @y += y
    @z += z
    PageBus.publish("#{@channel}.move", this)
    
  speak: (phrase) ->
    console.info "#{@name} said '#{phrase}'"
  tick: ->
    @processNeeds()
    @processTasks()
      
  processNeeds: ->
  processTasks: ->
    task = _.detect @task, (t) =>
      !t.pass(this)
    if task?
      task.perform(this)
      if task.is_done
        @task.splice @task.indexOf(task), 1
      true
    else
      false
      
  # true if successful, false otherwise
  putInBackpack: (item, amount=item.amount) ->
    if item.amount <= amount
      @backpack.push item
      item.pack()
      item.setContainer this
    else
      newItem = item.split(item.amount - amount)
      @backpack.push newItem
      newItem.setContainer this
    true

  hasInBackpack: (item, amount=item.amount) ->
    (_.include @backpack, item) && item.amount >= amount
    
  filterBackpack: (func) ->
    _.filter @backpack, func
  eachBackpack: (func) ->
    _.each @backpack, func
  
  # immediately transfer the item from the backpack to the destination
  # (doesn't create a DropOffItem task)
  # returns true if successful, false if unsuccessful, and null if creature didn't possess item
  removeFromBackpack: (item, destination="ground", amount=item.amount) ->
    backpackItemIdx = _.lastIndexOf @backpack, item
    return null if backpackItemIdx < 0
    success = false
    if amount >= item.amount
      backpackItem = @backpack.splice(backpackItemIdx, 1)[0]
      
      if destination instanceof Creature
        success = destination.putInBackpack backpackItem
      else if destination instanceof Fortress.Structure
        success = destination.putInContents backpackItem
          
      if destination == "ground" || !success
        item.x = @x
        item.y = @y
        item.z = @z
        item.unpack()
        item.setContainer null
    else
      throw new Error("not implemented yet")
      
    success

  # whether this creature will ever publish a move event
  canMove: ->
    true
      
class CreatureView extends Fortress.View
  constructor: ->
    super
    @z = 30
  paint: ->
    @needs_repaint = true
    super
    

window.Fortress.Creature = Creature
window.Fortress.CreatureView = CreatureView
