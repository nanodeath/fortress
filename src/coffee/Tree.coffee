class Tree extends Fortress.Structure
  constructor: (position, grown=true) ->
    super(position, Fortress.Civilization.get["Nature"], grown)

# softwood
class TreeDouglasFir extends Tree
  constructor: ->
    super
    @wood = 100
  description: ->
    "A Douglas Fir tree.  Good for chopping."
  getActions: ->
    [Fortress.ChopTreeTask]
    
class TreeDouglasFirView extends Fortress.StructureView
  constructor: -> 
    super
    @image = "img/tree.png"

# hardwood
class TreeApple extends Tree
  constructor: ->
    super
    @wood = 50
    @time_barren = 0
    @time_until_fruit = 20
    @fruit_count = 20
  description: ->
    "An Apple tree.  Produces fruit periodically."
  getActions: ->
    actions = [Fortress.ChopTreeTask]
    actions.push Fortress.HarvestFruitTask if @hasFruit()
    actions
  tick: ->
    if !@hasFruit()
      @time_barren++
      if @time_until_fruit <= @time_barren
        @putInContents new Fortress.Apple(@x, @y, @z, @fruit_count)
      
  hasFruit: ->
    @contents.length > 0
    
  getFruit: ->
    @contents[0]
    
  removeFromContents: ->
    super
    @time_barren = 0

class TreeAppleView extends Fortress.StructureView
  @barren_image = "img/appletree.noapples.png"
  @ripe_image = "img/appletree.png"
  constructor: -> 
    super
    @image = TreeAppleView.barren_image
    @barren = true
  paint: ->
    if @model.hasFruit()
      if @barren
        @img.set "src", TreeAppleView.ripe_image
        @barren = false
    else if !@barren
      @barren = true
      @img.set "src", TreeAppleView.barren_image
    super      

window.Fortress.Tree = Tree
window.Fortress.TreeDouglasFir = TreeDouglasFir
window.Fortress.TreeDouglasFirView = TreeDouglasFirView
window.Fortress.TreeApple = TreeApple
window.Fortress.TreeAppleView = TreeAppleView

