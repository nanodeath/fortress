class House extends Fortress.Structure
  constructor: ->
    super
    @time_to_build = 4
  setHealth: (@health) ->
    super
    if @health == 100
      console.log "stucture done!"
  resourcesRequired: ->
    [
      [Fortress.WoodPile, 50]
    ]

class HouseView extends Fortress.StructureView
  constructor: -> 
    super
    @image = "img/haunted_house.png"
    
window.Fortress.House = House
window.Fortress.HouseView = HouseView
